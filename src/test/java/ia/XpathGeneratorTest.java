package ia;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import controller.XpathGenerator;
import controller.XpathIdentifierController;
import io.github.bonigarcia.wdm.WebDriverManager;
import utils.XpathType;

public class XpathGeneratorTest {
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void createXpathBySiblingTest() {
		driver.get("http://www.google.com");
		String xpath = "//input[@name='q']";
		WebElement element = driver.findElement(By.xpath(xpath));
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		XpathGenerator xg = new XpathGenerator(element, driver);
		String result = xg.createXpathBySibling();
		System.out.println("XPATH BY Sibling:" + result);
		assert (new XpathIdentifierController(result).getXpathTypeName() == XpathType.BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH
				.name());
	}
	
	@Test
	public void createXpathBySiblingTest2() {
		driver.get("http://www.facebook.com");
		String xpath = "//*[@id='email']";
		WebElement element = driver.findElement(By.xpath(xpath));
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		XpathGenerator xg = new XpathGenerator(element, driver);
		String result = xg.createXpathBySibling();
		System.out.println("XPATH BY Sibling:" + result);
		if (result.isEmpty())
			System.out.println("XPATH BY SIBLING N�O PODE SER ENCONTRADO");
		assert (new XpathIdentifierController(result).getXpathTypeName() == XpathType.BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH
				.name());
	}

	@Test
	public void createXpathByParentTest() {
		driver.get("http://www.google.com");
		String xpath = "//input[@name='q']";
		WebElement element = driver.findElement(By.xpath(xpath));
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		XpathGenerator xg = new XpathGenerator(element, driver);

		String result = xg.createXpathByParent();
		System.out.println("XPATH BY PARENT:" + result);
		assert (new XpathIdentifierController(result).getXpathTypeName() == XpathType.BY_PARENT_ATTRIBUTE_ELEMENT_XPATH
				.name());
	}

	@Test
	public void createXpathByChildTest() {
		driver.get("http://www.google.com");
		String xpath = "//input[@name='q']";
		WebElement element = driver.findElement(By.xpath(xpath));
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		XpathGenerator xg = new XpathGenerator(element, driver);

		String result = xg.createXpathByChild();
		System.out.println("XPATH BY CHILD:" + result);
		assert (new XpathIdentifierController(result).getXpathTypeName() == XpathType.NONE_XPATH.name());
	}
	
	@Test
	public void createXpathByChildTest2() {
		driver.get("http://www.facebook.com");
//		String xpath = "//*[@id='fullname_field']/div[1]/div[1]";
		String xpath = "//*[@id='fullname_field']/div[1]";
		WebElement element = driver.findElement(By.xpath(xpath));
		XpathIdentifierController xic = new XpathIdentifierController(xpath);

		XpathGenerator xg = new XpathGenerator(element, driver);

		String result = xg.createXpathByChild();
		System.out.println("XPATH BY CHILD:" + result);
		assert (new XpathIdentifierController(result).getXpathTypeName() == XpathType.BY_SON_ELEMENT_ATTRIBUTE_XPATH.name());
	}

	@Test
	public void createXpathByAttributeTest() {
		driver.get("http://www.google.com");
		String xpath = "//input[@name='q']";
		WebElement element = driver.findElement(By.xpath(xpath));
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		XpathGenerator xg = new XpathGenerator(element, driver);

		String result = xg.createXpathByAttribute();
		System.out.println("XPATH BY ATTRIBUTE:" + result);
		assert (new XpathIdentifierController(result).getXpathTypeName() == XpathType.BY_ATTRIBUTE_ELEMENT_XPATH.name());
	}	

	@Test
	public void createXpathByParentTest2() {
		driver.get("http://www.facebook.com");
		String xpath = "//*[@id='email']";
		WebElement element = driver.findElement(By.xpath(xpath));
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		XpathGenerator xg = new XpathGenerator(element, driver);

		String result = xg.createXpathByParent();
		System.out.println("XPATH BY PARENT:" + result);
		if (result.isEmpty())
			System.out.println("XPATH BY PARENT N�O PODE SER ENCONTRADO");
		assert (new XpathIdentifierController(result).getXpathTypeName() == XpathType.BY_PARENT_ATTRIBUTE_ELEMENT_XPATH
				.name());
	}

	
}
