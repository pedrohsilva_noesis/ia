package ia;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import controller.XpathIdentifierController;
import io.github.bonigarcia.wdm.WebDriverManager;
import utils.XpathType;

public class XpathIdentifierTest {
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void byAttributeElementXpathTest() {
		driver.get("http://www.google.com");
		String xpath = "//input[@name='q']";
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		assertEquals(xic.getXpathTypeName(), XpathType.BY_ATTRIBUTE_ELEMENT_XPATH.name());
	}

	@Test
	public void byParentElementXpathTest() {
		driver.get("http://www.google.com");
		String xpath = "//div[@jsname='gLFyf']/input";
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		assertEquals(xic.getXpathTypeName(), XpathType.BY_PARENT_ATTRIBUTE_ELEMENT_XPATH.name());
	}

	@Test
	public void bySonElementXpathTest() {
		driver.get("http://www.google.com");
		String queryXpath = "//div[@jsname='gLFyf']/input";
		String okXpath = "(//input[@name=\"btnK\"])[2]";
		String h3Xpath = "//h3[@class='LC20lb DKV0Md' and contains(.,'Base de Dados')]/parent::a";
		XpathIdentifierController xic = new XpathIdentifierController(h3Xpath);

		driver.findElement(By.xpath(queryXpath)).sendKeys("Pedro");
		driver.findElement(By.xpath(okXpath)).submit();
		assertEquals(xic.getXpathTypeName(), XpathType.BY_SON_ELEMENT_ATTRIBUTE_XPATH.name());

	}
	
	@Test
	public void bySiblingElementXpathTest() {
		driver.get("http://www.google.com");
		String xpath = "//div[@jsname='vdLsw']/following-sibling::input";
		XpathIdentifierController xic = new XpathIdentifierController(xpath);
		assertEquals(xic.getXpathTypeName(), XpathType.BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH.name());
	}
}
