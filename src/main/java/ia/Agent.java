package ia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import controller.XpathGenerator;
import controller.XpathIdentifierController;
import dao.RewardDAO;
import model.Reward;
import service.RewardService;
import utils.XpathType;
import static utils.Constants.*;

public class Agent {

	private String currentXpath;
	private int idTestStep;
	private RemoteWebDriver driver;
	private RewardService rewardService;

	public Agent(int idTestStep, String xpath, RemoteWebDriver driver) {
		this.currentXpath = xpath;
		this.idTestStep = idTestStep;
		this.driver = driver;
		rewardService = new RewardService();
	}

//	private Integer[] possibleXpathTypes(int currentState) {
//		// Retorna todos os tipos de xpaths diferentes do atual
//		ArrayList<Integer> listXpathTypes = (ArrayList<Integer>) Arrays.asList(STATES);
//		listXpathTypes.remove(new Integer(currentState));
//		return (Integer[]) listXpathTypes.toArray();
//
//	}

	public String getNewXpathToTrainning() {
		// dado um xpath para um step, cria todas as equival�ncias na base se n�o
		// existir
		// retornar� a menos utilizada.
		List<String> newXpathList = generateAllXpaths(currentXpath);
		for (Iterator iterator = newXpathList.iterator(); iterator.hasNext();) {
			String xpath = (String) iterator.next();
			rewardService.persist(new Reward(currentXpath, xpath, 0, idTestStep), false);
		}
		return rewardService.getLessUsedReward(idTestStep).getNew_xpath();
	}

	public void persistTrainningResult(String newXpath, boolean success, int executionTime) {
		// recebe o retorno da execu��o e persiste a recompensa
		int reward = calculateReward(newXpath, success, executionTime);
		Reward r = new Reward(currentXpath, newXpath, reward, idTestStep);
		rewardService.persist(r, true);
	}

//	public void run() {
//		/*
//		 * Dado o estado atual, escolho aleatoriamente entre explorar qqr xpath (20%,
//		 * por exemplo) e utilizar o xpath mais bem sucedido (80%)
//		 * 
//		 * As op��es de estado s�o todas menos a atual.
//		 * 
//		 * Se o xpath escolhido tiver sucesso, receber� recompensa. Mas se ele for
//		 * extenso, ter� desconto. Se der falha, ter� penalidade.
//		 * 
//		 * Dever� ter um tabela que registre os xpaths e suas pontua��es.
//		 */
//		int xpathType = new XpathIdentifierController(currentXpath).getXpathTypeValue();
//		Map<String, Integer> result = new HashMap<String, Integer>();
//		Integer[] possibleXpathTypes = possibleXpathTypes(xpathType);
//		for (int i = 0; i < NUMBER_TRAINING_CYCLES; i++) {
//			result = executeTrainingCycle(possibleXpathTypes, xpathType, result);
//			persistTrainingCycle(result);
//		}
//	}

//	private Map<String, Integer> executeTrainingCycle(Integer[] possibleXpathTypes, int xpathType,
//			Map<String, Integer> result) {
//		int executionTime = 0;
//		int reward = 0;
//		String newXpath = "";
//		String bestXpath = "";
//		String worstXpath = "";
//		Integer bestTime = 0;
//		Integer worstTime = 0;
//		for (int j = 0; j < possibleXpathTypes.length; j++) {
//			newXpath = generateNewXpath(currentXpath, possibleXpathTypes[j]);
//			executionTime = executeTest(newXpath);
//			reward = calculateReward(executionTime, newXpath);
//			if (executionTime > worstTime) {
//				worstTime = executionTime;
//				worstXpath = newXpath;
//			}
//			if (executionTime < bestTime) {
//				bestTime = executionTime;
//				bestXpath = newXpath;
//			}
//			result.put(newXpath, reward);
//		}
//		return calculateRewardByPerformance(result, bestXpath, worstXpath);
//	}

	private Map<String, Integer> calculateRewardByPerformance(Map<String, Integer> result, String bestXpath,
			String worstXpath) {
		int bestXpathReward = result.remove(bestXpath);
		int worstXpathReward = result.remove(worstXpath);
		result.put(bestXpath, bestXpathReward + MAX_REWARD_BY_PERFORMANCE);
		result.put(worstXpath, worstXpathReward + MAX_PENALTY_BY_PERFORMANCE);
		return result;
	}

//	private void persistTrainingCycle(Map<String, Integer> result) {
//		// result tem (xpath, recompensa), o current xpath ser� usado na base
//		for (Iterator iterator = result.keySet().iterator(); iterator.hasNext();) {
//			String xpath = (String) iterator.next();
//			rewardService.persist(new Reward(currentXpath, xpath, result.get(xpath), idTestStep));
//		}
//	}

	private int calculateReward(String newXpath, boolean success, int executionTime) {
		if (!success) {
			return PENALTY_BY_FAIL;
		} else {
			return REWARD + getDeepness(newXpath) - getPerformance(executionTime);
		}
	}

	private int getPerformance(int executionTime) {
		// quanto menor o tempo, menos pontos ser�o tirados
		/*
		 * 60S = 36 PNTS; 62S = 38 PNTS; 65S = 42 PNTS; 70S = 49 PNTS ; 90S = 81 PNTS
		 */
		return (executionTime / 10) ^ 2;
	}

	public String getSuggestion(int id_test_step) {
		return rewardService.getBiggerReward(id_test_step).getNew_xpath();
	}
//
//	private String generateNewXpath(String xpath, int xpathType) {
//		WebElement we = driver.findElement(By.xpath(xpath));
//		XpathGenerator xg = new XpathGenerator(we, driver);
//		if (xpathType == XpathType.ABSOLUTE_XPATH.getValue()) {
//			return xg.createAbsoluteXpath();
//		}
//		if (xpathType == XpathType.BY_ATTRIBUTE_ELEMENT_XPATH.getValue()) {
//			return xg.createXpathByAttribute();
//		}
//		if (xpathType == XpathType.BY_PARENT_ATTRIBUTE_ELEMENT_XPATH.getValue()) {
//			return xg.createXpathByParent();
//		}
//		if (xpathType == XpathType.BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH.getValue()) {
//			return xg.createXpathBySibling();
//		}
//		if (xpathType == XpathType.BY_SON_ELEMENT_ATTRIBUTE_XPATH.getValue()) {
//			return xg.createXpathByChild();
//		}
//		return "";
//	}

	private List<String> generateAllXpaths(String xpath) {
		WebElement we = driver.findElement(By.xpath(xpath));
		XpathGenerator xg = new XpathGenerator(we, driver);
		List<String> list = new ArrayList<String>();
		list.add(xg.createAbsoluteXpath());
		list.add(xg.createXpathByAttribute());
		list.add(xg.createXpathByParent());
		list.add(xg.createXpathBySibling());
		list.add(xg.createXpathByChild());
		return list;
	}

	private int getDeepness(String xpath) {
		xpath = xpath.replace("//", "");
		int count = 0;
		for (int i = 0; i < xpath.length(); i++) {
			if (xpath.charAt(i) == '/')
				count++;
		}
		return count * PENALTY_BY_DEEPNESS;
	}

	public static void main(String[] args) {
		// 1 - A ia deve receber uma lista de casos de teste para serem executados
		// 2 - Antes de executar o primeiro, ela deve alterar o xpath. Altera todos os
		// xpaths do caso e executa
		// 3 - Depois avalia o status de cada step executado e atualiza na base
		// 4 - Ap�s analisar o caso, avaliar o pr�ximo.
		// Estes passos de execu��o devem estar numa outra classe respons�vel (fora do
		// agente).
		Integer[] testStepList = new Integer[10];

		int idTestStep = 5;
		String xpath = "";
		RemoteWebDriver driver = null;

		Agent a = new Agent(idTestStep, xpath, driver);
	}

}