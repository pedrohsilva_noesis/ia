package utils;

public class Constants {
	public static final int REWARD = 100;
	public static final int MAX_REWARD_BY_PERFORMANCE = 10;
	public static final int PENALTY_BY_FAIL = -50;
	public static final int PENALTY_BY_DEEPNESS = -2;
	public static final int MAX_PENALTY_BY_PERFORMANCE = -10;
	public static final int NUMBER_TRAINING_CYCLES = 10;
	public static Integer[] STATES = { XpathType.ABSOLUTE_XPATH.getValue(),
			XpathType.BY_ATTRIBUTE_ELEMENT_XPATH.getValue(), XpathType.BY_PARENT_ATTRIBUTE_ELEMENT_XPATH.getValue(),
			XpathType.BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH.getValue(),
			XpathType.BY_SON_ELEMENT_ATTRIBUTE_XPATH.getValue() };
	// SECURITY
	public static String PERMISSIONS_ON = "false";
	public static String ADMIN_ROLE = "Administrators";
	public static String ADMIN_GROUP = "jira-administrators";
	public static String DASHBOARD_GROUP = "jira-ntx-dashboard";

	// Database
	public static String DB_TYPE;
	public static String DB_HOST_IP;
	public static String DB_NAME;
	public static String DB_USER_ID;
	public static String DB_CREDENTIAL;
	public static String DB_INSTANCE_PORT = "1433";
	public static String DB_INSTANCE_NAME;
	public static String DB_PERMISSIONS_TIMEOUT = "30";
	public static final String DB_TYPE_ORACLE = "oracle";
	public static final String DB_TYPE_ORACLE_LDAP = "oracle_ldap";
	public static final String DB_TYPE_MYSQL = "mysql";
	public static final String DB_TYPE_SQLSERVER = "sqlserver";
	public static final String ERROR = "Error";

}
