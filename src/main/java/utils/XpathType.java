package utils;

public enum XpathType {
	ABSOLUTE_XPATH(0), RELATIVE_XPATH(1), BY_ATTRIBUTE_ELEMENT_XPATH(2), BY_PARENT_ATTRIBUTE_ELEMENT_XPATH(3),
	BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH(4), BY_SON_ELEMENT_ATTRIBUTE_XPATH(5), NONE_XPATH(6);

	private int value;

	XpathType(int i) {
		value = i;
	}

	public int getValue() {
		return value;
	}
	
	public String getXpathTypeName() {
		switch (getValue()) {
		case 0:
			return ABSOLUTE_XPATH.name();
		case 1:
			return RELATIVE_XPATH.name();
		case 2:
			return BY_ATTRIBUTE_ELEMENT_XPATH.name();
		case 3:
			return BY_PARENT_ATTRIBUTE_ELEMENT_XPATH.name();
		case 4:
			return BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH.name();
		case 5:
			return BY_SON_ELEMENT_ATTRIBUTE_XPATH.name();
		default:
			return NONE_XPATH.name();
		}
	}
	
	public boolean isByAbsoluteXpath() {
		return value == ABSOLUTE_XPATH.getValue();
	}
	
	public boolean isByRelativeXpath() {
		return value == RELATIVE_XPATH.getValue();
	}
	
	public boolean isByAttributeElementXpath() {
		return value == BY_ATTRIBUTE_ELEMENT_XPATH.getValue();
	}
	
	public boolean isByParentElementXpath() {
		return value == BY_PARENT_ATTRIBUTE_ELEMENT_XPATH.getValue();
	}
	public boolean isBySiblingElementXpath() {
		return value == BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH.getValue();
	}
	public boolean isBySonElementXpath() {
		return value == BY_SON_ELEMENT_ATTRIBUTE_XPATH.getValue();
	}
	
	
	
	
	
	
	
	
	
	
}
