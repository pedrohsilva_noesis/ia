package utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import controller.AbsoluteXpathGenerator;

public class FamilyXpath {
	public static WebElement getParent(WebElement child) {
		return child.findElement(By.xpath(".."));
//		return (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].parentNode;", child);
	}

	public static List<WebElement> getSiblings(WebElement child) {
		WebElement parentElement = child.findElement(By.xpath(".."));
		return parentElement.findElements(By.xpath("*"));
	}
	
	public static List<WebElement> getImmediateChildren(WebElement element) {
		return element.findElements(By.xpath("./*"));
	}
	
	public static List<WebElement> getAllDescendents(WebElement element) {
		return element.findElements(By.xpath(".//*"));
	}
	
	public static String getIdentifierAttribute(WebDriver driver, WebElement we) {
		List<String> attributes = getElementAttributes((RemoteWebDriver)driver, AbsoluteXpathGenerator.create(we));
		String xpath = "";
		for (Iterator iterator = attributes.iterator(); iterator.hasNext();) {
			String attr = (String) iterator.next();
			String value = we.getAttribute(attr);
			String xp = "//" + we.getTagName() + "[@" + attr + " = '" + value + "']";
			if (driver.findElements(By.xpath(xp)).size() == 1) {
				xpath = xp;
				break;
			}
		}
		return xpath;
	}
	
	public static List<String> getIdentifierAttributeList(WebDriver driver, WebElement we) {
		List<String> attributes = getElementAttributes((RemoteWebDriver)driver, AbsoluteXpathGenerator.create(we));
		List<String> xpaths = new ArrayList<String>();
		String xpath = "";
		for (Iterator iterator = attributes.iterator(); iterator.hasNext();) {
			String attr = (String) iterator.next();
			String value = we.getAttribute(attr);
			String xp = "//" + we.getTagName() + "[@" + attr + " = '" + value + "']";
			if (driver.findElements(By.xpath(xp)).size() == 1) {
				xpath = xp;
				xpaths.add(xpath);
			}
		}
		return xpaths;
	}
	
	public static List<String> getElementAttributes(RemoteWebDriver driver, String xpath) {
		// APOS RECUPERAR O JAVA SCRIPT, RETORNA UM STRING COM TODOS OS ATRIBUTOS
		String script = "function nasa() {return document.evaluate(" + "\"" + xpath + "\""
				+ ", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;}  return new XMLSerializer().serializeToString(nasa());";
		String ret = driver.executeScript(script).toString();
		ret = ret.substring(0, ret.indexOf('>') + 1);
		ret = ret.replace("=\"", "=").replace("<", "").replace("/>", "").replace(">", "");
		String ret2[] = ret.split("\" ");
		List<String> ret3 = new ArrayList<String>();
		for (int i = 1; i < ret2.length; i++) {
			String string = ret2[i];
			int equals = string.indexOf("=");
			ret3.add(string.substring(0, equals));
		}
		return ret3;
	}
}
