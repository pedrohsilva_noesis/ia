package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.Constants;
import utils.DbUtils;


/**
 * Constructor
 *
 *
 */
public class Database {

    private static final String DB_DRIVER_MYSQL = "com.mysql.cj.jdbc.Driver";
    private static final String DB_DRIVER_ORACLE = "oracle.jdbc.OracleDriver";
    private static final String DB_DRIVER_SQLSERVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    private static final String DB_CONNECTION_MYSQL = "jdbc:mysql://" + Constants.DB_HOST_IP + ":3306" + "/" + Constants.DB_NAME + "?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC&relaxAutoCommit=true";
    private static final String DB_CONNECTION_ORACLE = "jdbc:oracle:thin:@//" + Constants.DB_HOST_IP + ":1521/" + Constants.DB_NAME;
    private static final String DB_CONNECTION_ORACLE_LDAP = "jdbc:oracle:thin:@ldap://" + Constants.DB_HOST_IP + "/" + Constants.DB_NAME;
    private static final String DB_CONNECTION_SQLSERVER = "jdbc:sqlserver://" + Constants.DB_HOST_IP + (!Constants.DB_INSTANCE_NAME.equals("") ? (";instanceName=" + Constants.DB_INSTANCE_NAME) : (":" + Constants.DB_INSTANCE_PORT)) + ";databaseName=" + Constants.DB_NAME + ";integratedSecurity=true";

    private static Connection connection = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(Database.class);

    /**
     * New connection instance
     *
     * @return
     * @throws java.sql.SQLException
     * @throws Exception
     */
    public static Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed() == true) {
            connection = openConnection();
        }
        return connection;
    }

    /**
     * Open new connection
     *
     * @throws Exception
     */
    private static Connection openConnection() throws SQLException {
        Connection dbConnection = null;
        try {

            if (Constants.PERMISSIONS_ON.equals("true")) {
                DriverManager.setLoginTimeout(Integer.parseInt(Constants.DB_PERMISSIONS_TIMEOUT));
            }

            switch (Constants.DB_TYPE) {
                case Constants.DB_TYPE_MYSQL:
                    Class.forName(DB_DRIVER_MYSQL);
                    dbConnection = DriverManager.getConnection(DB_CONNECTION_MYSQL, Constants.DB_USER_ID, Constants.DB_CREDENTIAL);
                    break;
                case Constants.DB_TYPE_ORACLE:
                    Class.forName(DB_DRIVER_ORACLE);
                    dbConnection = DriverManager.getConnection(DB_CONNECTION_ORACLE, Constants.DB_USER_ID, Constants.DB_CREDENTIAL);
                    break;
                case Constants.DB_TYPE_ORACLE_LDAP:
                    Class.forName(DB_DRIVER_ORACLE);
                    dbConnection = DriverManager.getConnection(DB_CONNECTION_ORACLE_LDAP, Constants.DB_USER_ID, Constants.DB_CREDENTIAL);
                    break;
                case Constants.DB_TYPE_SQLSERVER:
                    Class.forName(DB_DRIVER_SQLSERVER);
                    dbConnection = DriverManager.getConnection(DB_CONNECTION_SQLSERVER);
                    break;
                default:
                    break;
            }
        } catch (ClassNotFoundException e) {
            switch (Constants.DB_TYPE) {
                case Constants.DB_TYPE_MYSQL:
                    LOGGER.error("Driver not found <" + DB_DRIVER_MYSQL + "> " + e.getMessage(), e);
                    break;
                case Constants.DB_TYPE_ORACLE:
                case Constants.DB_TYPE_ORACLE_LDAP:
                    LOGGER.error("Driver not found <" + DB_DRIVER_ORACLE + "> " + e.getMessage(), e);
                    break;
                case Constants.DB_TYPE_SQLSERVER:
                    LOGGER.error("Driver not found <" + DB_DRIVER_SQLSERVER + "> " + e.getMessage(), e);
                    break;
                default:
                    break;
            }
        } catch (SQLException e) {
            LOGGER.error(Constants.ERROR + e.getMessage(), e);
            throw new SQLException("Error occurred while getting the database connection, " + e.getMessage());
        }

        return dbConnection;

    }

    public static List<String> QueryDB(String query, String dataColumnType, String Aux) {
        List<String> result = new ArrayList<>();
        Statement st = null;
        ResultSet rs = null;
        try {
            if (!"".equals(Aux)) {
                result.add(Aux);
            }
            Connection mySQLconn = Database.getConnection();
            st = mySQLconn.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                String strKeyword = rs.getString(dataColumnType);
                result.add(strKeyword);
            }
        } catch (SQLException e) {
            LOGGER.error("Error QueryDB " + e.getMessage(), e);
        } catch (NullPointerException e) {
            LOGGER.error("Error connection QueryDB " + e.getMessage(), e);
        } finally {
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(rs);
        }
        return result;
    }
}
