package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.Reward;
import utils.DbUtils;

public class RewardDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(RewardDAO.class);
	private Connection mySQLconn;

	public RewardDAO() throws SQLException {
		mySQLconn = Database.getConnection();
	}

	public boolean existReward(Reward r) {
		String query = "SELECT count(*) as num_regs FROM tbl_ia where fk_id_test_step = ? and old_xpath = ? and new_xpath = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = mySQLconn.prepareStatement(query);
			ps.setInt(1, r.getFk_id_test_step());
			ps.setString(2, r.getOld_xpath());
			ps.setString(3, r.getNew_xpath());
			rs = ps.executeQuery(query);
			if (rs.next()) {
				return rs.getInt("num_regs") > 0;
			}
		} catch (SQLException e) {
			String message = ((e.getMessage() == null || e.getMessage().isEmpty()) ? e.toString() : e.getMessage());
			LOGGER.error("Error SQLException: " + message, e);
		} finally {
			DbUtils.closeQuietly(ps);
			DbUtils.closeQuietly(rs);
		}
		return false;
	}

	public void persistNewReward(Reward r) {
		String query = "INSERT INTO db_ntx.tbl_ia(old_xpath, new_xpath, attempts, reward, fk_id_test_step) VALUES(?,?,1,?,?)";
		PreparedStatement ps = null;
		try {
			ps = mySQLconn.prepareStatement(query);
			ps.setString(1, r.getOld_xpath());
			ps.setString(2, r.getNew_xpath());
			ps.setInt(3, r.getReward());
			ps.setInt(4, r.getFk_id_test_step());
			ps.execute(query);

		} catch (SQLException e) {
			String message = ((e.getMessage() == null || e.getMessage().isEmpty()) ? e.toString() : e.getMessage());
			LOGGER.error("Error SQLException: " + message, e);
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	public void updateReward(Reward r, boolean updateAttemps) {
		String atmp="";
		if(updateAttemps)
			atmp = "attempts = attempts+1,";
		String query = "update db_ntx.tbl_ia set old_xpath = ?, new_xpath = ?, "+atmp+" reward = reward + ?, fk_id_test_step = ?"
				+ " where id_ia = ?";
		PreparedStatement ps = null;
		try {
			ps = mySQLconn.prepareStatement(query);
			ps.setString(1, r.getOld_xpath());
			ps.setString(2, r.getNew_xpath());
			ps.setInt(3, r.getReward());
			ps.setInt(4, r.getFk_id_test_step());
			ps.setInt(5, r.getId_ia());
			ps.execute(query);

		} catch (SQLException e) {
			String message = ((e.getMessage() == null || e.getMessage().isEmpty()) ? e.toString() : e.getMessage());
			LOGGER.error("Error SQLException: " + message, e);
		} finally {
			DbUtils.closeQuietly(ps);
		}
	}

	public Reward getBiggerReward(int id_test_step) {
		String query = "SELECT max(reward), id_ia, old_xpath, new_xpath, attempts, reward"
				+ "  FROM tbl_ia where fk_id_test_step = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = mySQLconn.prepareStatement(query);
			ps.setInt(1, id_test_step);
			rs = ps.executeQuery(query);
			if (rs.next()) {
				Reward r = new Reward(rs.getString("old_xpath"), rs.getString("new_xpath"), rs.getInt("attempts"),
						id_test_step);
				r.setId_ia(rs.getInt("id_ia"));
				r.setIs_activated(rs.getInt("is_activated") == 1);
				return r;
			}
		} catch (SQLException e) {
			String message = ((e.getMessage() == null || e.getMessage().isEmpty()) ? e.toString() : e.getMessage());
			LOGGER.error("Error SQLException: " + message, e);
		} finally {
			DbUtils.closeQuietly(ps);
			DbUtils.closeQuietly(rs);
		}
		return null;
	}

	public Reward getLessUsedReward(int id_test_step) {
		String query = "SELECT min(attempts), id_ia, old_xpath, new_xpath, attempts, reward"
				+ "  FROM tbl_ia where fk_id_test_step = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = mySQLconn.prepareStatement(query);
			ps.setInt(1, id_test_step);
			rs = ps.executeQuery(query);
			if (rs.next()) {
				Reward r = new Reward(rs.getString("old_xpath"), rs.getString("new_xpath"), rs.getInt("attempts"),
						id_test_step);
				r.setId_ia(rs.getInt("id_ia"));
				r.setIs_activated(rs.getInt("is_activated") == 1);
				return r;
			}
		} catch (SQLException e) {
			String message = ((e.getMessage() == null || e.getMessage().isEmpty()) ? e.toString() : e.getMessage());
			LOGGER.error("Error SQLException: " + message, e);
		} finally {
			DbUtils.closeQuietly(ps);
			DbUtils.closeQuietly(rs);
		}
		return null;
	}

}
