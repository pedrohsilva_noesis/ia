package dao;

import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.TestStep;
import utils.DbUtils;

public class TestStepDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(TestStepDAO.class);
	private Connection mySQLconn;

	public TestStepDAO() throws SQLException {
		mySQLconn = Database.getConnection();
	}

	public List<Integer> getMonitoredTests() {
		String query = "select distinct fk_test_id as id from tbl_test_steps where fk_id_step_used_by_ia <> null;"; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Integer> listTestId = new ArrayList<Integer>();
		try {
			ps = mySQLconn.prepareStatement(query);
			rs = ps.executeQuery(query);
			while (rs.next()) {
				listTestId.add(rs.getInt("id"));
			}
		} catch (SQLException e) {
			String message = ((e.getMessage() == null || e.getMessage().isEmpty()) ? e.toString() : e.getMessage());
			LOGGER.error("Error SQLException: " + message, e);
		} finally {
			DbUtils.closeQuietly(ps);
			DbUtils.closeQuietly(rs);
		}
		return listTestId;
	}

	public List<TestStep> getTestStep(Integer idTest) {
		String query = "select \r\n" + 
				"ts.step_id, \r\n" + 
				"ts.fk_object_id,\r\n" + 
				"o.xpath,\r\n" + 
				"o.xpath_iframe \r\n" + 
				"from tbl_test_steps ts \r\n" + 
				"inner join tbl_objects o on o.object_id = ts.fk_object_id\r\n" + 
				"where ts.fk_test_id=13 and ts.fk_template_id = null and o.isWeb = 1 and ts.active = 1\r\n" + 
				"order by ordem asc;"; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TestStep> listTestStep = new ArrayList<TestStep>();
		try {
			ps = mySQLconn.prepareStatement(query);
			rs = ps.executeQuery(query);
			while (rs.next()) {
				TestStep ts = new TestStep();
				ts.setFk_object_id(rs.getInt("fk_object_id"));
				ts.setStep_id(rs.getInt("step_id"));
				ts.setXpath(rs.getString("xpath"));
				ts.setXpath_iframe(rs.getString("xpath_iframe"));
				listTestStep.add(ts);
			}
		} catch (SQLException e) {
			String message = ((e.getMessage() == null || e.getMessage().isEmpty()) ? e.toString() : e.getMessage());
			LOGGER.error("Error SQLException: " + message, e);
		} finally {
			DbUtils.closeQuietly(ps);
			DbUtils.closeQuietly(rs);
		}
		return listTestStep;
		  
	}

}
