package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.FamilyXpath;

public class RelativeXpathGenerator {
	public static int LEVEL_LIMIT = 5;
	public static int LEVEL_START = 1;

/*
 * Sentido: Procura o elemento pelo atributo identificador. Se n�o encontrar, desce a �rvore at� 5 gera��es
 * At� que encontre um filho com o atributo identificador
 */
	public static String create(WebDriver driver, WebElement webElement) {
		return RelativeXpathGenerator.create(driver, webElement, new ArrayList<String>());
	}

	public static String create(WebDriver driver, WebElement webElement, List<String> ignoredXpaths) {
		// Retorna atributo capaz de identificar o elemento. Caso n�o encontre um
		// relativo criado atrav�s de um atributo, procura dentro dos filhos at� a 5a geracao

		List<String> listXpathByAttribute = FamilyXpath.getIdentifierAttributeList(driver, webElement);
		if (listXpathByAttribute.isEmpty())
			return getReliableDescendant(driver, webElement, LEVEL_START);
		if (ignoredXpaths.isEmpty()) {
			return listXpathByAttribute.get(0);
		}

		for (Iterator iterator = listXpathByAttribute.iterator(); iterator.hasNext();) {
			String xp = (String) iterator.next();
			if (!ignoredXpaths.contains(xp))
				return xp;
		}
		return "";

	}

	private static String mountXpathByAttribute(WebElement webElement, String attribute) {
		return "//" + webElement.getTagName() + "[@" + attribute + "=" + "'" + webElement.getAttribute(attribute) + "'"
				+ "]";
	}

	private static String getReliableDescendant(WebDriver driver, WebElement element, int level) {
		String xpath = "";
		if (level == LEVEL_LIMIT) {
			return "";
		} else {
			List<WebElement> listChildren = FamilyXpath.getImmediateChildren(element);
			for (Iterator iterator = listChildren.iterator(); iterator.hasNext();) {
				WebElement child = (WebElement) iterator.next();
				xpath = create(driver, child);
				if (!xpath.isEmpty()) {
					xpath = xpath + "/parent::" + element.getTagName();
					return xpath;
				} else {
					return getReliableDescendant(driver, child, level + 1);
				}
			}
		}
		if (xpath.isEmpty())
			return xpath;
		return xpath + "/parent::" + element.getTagName();
	}
	
}
