package controller;

import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.FamilyXpath;

public class XpathGenerator {
	WebElement we;
	WebDriver driver;

	public XpathGenerator(WebElement we, WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.we = we;
		this.driver = driver;
	}

	public String createXpathByChild() {
		// Procurar o elemento filho para acessar o elemento desejado
		// Exemplo: //div/input neste caso eu pesquiso o input para acessar a div
		List<WebElement> children = FamilyXpath.getImmediateChildren(we);
		String xpath = "";
		for (Iterator iterator = children.iterator(); iterator.hasNext();) {
			WebElement webElement = (WebElement) iterator.next();
			xpath = RelativeXpathGenerator.create(driver,webElement);
			if (!xpath.isEmpty()) {
				return xpath + "/parent::" + we.getTagName();
			}
		}
		return "";
	}

	public String createXpathBySibling() {
		// Procurar o elemento irm�o para acessar o elemento desejado
		WebElement parent = FamilyXpath.getParent(we);
		List<WebElement> weSiblings = parent.findElements(By.xpath(".//*"));
		weSiblings.remove(we);
		String identifierAttribute = "";
		for (Iterator iterator = weSiblings.iterator(); iterator.hasNext();) {
			WebElement webElement = (WebElement) iterator.next();
			identifierAttribute = RelativeXpathGenerator.create(driver,webElement);
			if (!identifierAttribute.isEmpty()) {
				return identifierAttribute + "/following-sibling::" + we.getTagName();
			}
		}
		return "";
	}

	public String createXpathByAttribute() {
		// Procurar o elemento atrav�s de um atributo identificador
		return RelativeXpathGenerator.create(driver, we);
	}
	
	public String createXpathByParent() {
		//captura por atributo e n�o tendo utilizar index at� achar um pai confi�vel
		return IndexedXpathGenerator.create(driver, we); 
	}

	public String createAbsoluteXpath() {
		return AbsoluteXpathGenerator.create(we);
	}
	
}
