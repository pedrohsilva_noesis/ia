package controller;

import utils.XpathType;

public class XpathIdentifierController {
	String xpath;

	public static final String SINGLE_SLASH = "/";
	public static final String DOUBLE_SLASH = "//";
	public static final String SLASH_HTML_SLASH = "/html/";
	public static final String ATTRIBUTE_SYMBOL = "@";
	public static final String SIBLING_REFERENCE = "following-sibling::";
	public static final String PARENT_REFERENCE = "parent::";

	public XpathIdentifierController(String xpath) {
		this.xpath = xpath;
	}

	private boolean isAbsolute() {
		// Validade se xpath � absoluto
		return !xpath.contains(DOUBLE_SLASH) & xpath.toLowerCase().startsWith(SLASH_HTML_SLASH);
	}

	private boolean isRelative() {
		// Valida se xpath � relativo
		return xpath.contains(DOUBLE_SLASH);
	}

	private boolean isBuiltByAttributeElement() {
		// Valida se xpath � baseado no atributo do elemento
		int elementIndex = xpath.lastIndexOf(SINGLE_SLASH);
		if (elementIndex < 0)
			return false;
		String xpathSufix = xpath.substring(elementIndex, xpath.length());
		return xpathSufix.contains(ATTRIBUTE_SYMBOL);
	}

	private boolean isBuiltByParentElement() {
		// Pesquisa se o xpath referencia algum atributo do elemento pai
		// Ele isola a referencia do pai do elemento para verificar se h� referencia a
		// atributo (@)
		int elementIndex = xpath.lastIndexOf(SINGLE_SLASH);
		if (elementIndex < 0)
			return false;
		String xpathPrefix = xpath.substring(0, elementIndex);
		elementIndex = xpathPrefix.lastIndexOf(SINGLE_SLASH);
		// o prefixo ter� ao menos 1 barra e 2 caracteres para compor uma tag, isto ser�
		// o pai do elemento
		return xpathPrefix.length() >= 3 & elementIndex >= 0;
//		return new XpathIdentifierController(xpathPrefix).isBuiltByAttributeElement();
	}

	private boolean isBuiltBySiblingElement() {
		// Pesquisa se o xpath referencia algum atributo do elemento irm�o
		// Na linha abaixo selecionamos o xpath at� antes da refer�ncia ao objeto
		int elementIndex = xpath.lastIndexOf(SINGLE_SLASH);
		if (elementIndex < 0)
			return false;
		String xpathSufix = xpath.substring(elementIndex, xpath.length());
		String xpathPrefix = xpath.substring(0, elementIndex);
		return xpathSufix.contains(SIBLING_REFERENCE);
	}

	private boolean isBuiltBySonElement() {
		// Pesquisa se o xpath referencia algum atributo do elemento filho
		int elementIndex = xpath.lastIndexOf(SINGLE_SLASH);
		if (elementIndex < 0)
			return false;
		String xpathSufix = xpath.substring(elementIndex, xpath.length());
		String xpathPrefix = xpath.substring(0, elementIndex);
		return xpathSufix.contains(PARENT_REFERENCE);
	}

	public String getXpathTypeName() {
		if (isAbsolute())
			return XpathType.ABSOLUTE_XPATH.name();
		if (isBuiltBySiblingElement())
			return XpathType.BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH.name();
		if (isBuiltBySonElement())
			return XpathType.BY_SON_ELEMENT_ATTRIBUTE_XPATH.name();
		if (isBuiltByParentElement())
			return XpathType.BY_PARENT_ATTRIBUTE_ELEMENT_XPATH.name();
		if (isBuiltByAttributeElement())
			return XpathType.BY_ATTRIBUTE_ELEMENT_XPATH.name();
		if (isRelative())
			return XpathType.RELATIVE_XPATH.name();
		return XpathType.NONE_XPATH.name();
	}

	public int getXpathTypeValue() {
		if (isAbsolute())
			return XpathType.ABSOLUTE_XPATH.getValue();
		if (isBuiltBySiblingElement())
			return XpathType.BY_SIBLING_ELEMENT_ATTRIBUTE_XPATH.getValue();
		if (isBuiltBySonElement())
			return XpathType.BY_SON_ELEMENT_ATTRIBUTE_XPATH.getValue();
		if (isBuiltByParentElement())
			return XpathType.BY_PARENT_ATTRIBUTE_ELEMENT_XPATH.getValue();
		if (isBuiltByAttributeElement())
			return XpathType.BY_ATTRIBUTE_ELEMENT_XPATH.getValue();
		if (isRelative())
			return XpathType.RELATIVE_XPATH.getValue();
		return XpathType.NONE_XPATH.getValue();
	}
}
