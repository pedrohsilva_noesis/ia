package controller;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.FamilyXpath;

public class IndexedXpathGenerator{
	/*Sentido: Achar o pai e descer a �rvore at� o filho
	 * Se o pai n�o tem atributo identificador, ser� utilizado o �ndice
	 * 
	 * 
	 */
	public static String create(WebDriver driver, WebElement child) {
		String xpath = "", xpathParent = "", xpathChild = "";
		WebElement parent = null;
		boolean stopLoop = false;
		while (!stopLoop) {
			parent = FamilyXpath.getParent(child);
			xpathParent = FamilyXpath.getIdentifierAttribute(driver, parent);
			if (!xpathParent.isEmpty()) {
				stopLoop = true;
			}
			List<WebElement> elementSiblings = FamilyXpath.getSiblings(child);
			xpathChild = mountXpathChild(elementSiblings, xpathChild, child);
			xpath = xpathParent + "/" + xpathChild;
			child = parent;
		}
		return xpath;
	}

	private static String mountXpathChild(List<WebElement> elements, String xpathChild, WebElement child) {
		if (elements.isEmpty()) {
			if (xpathChild.isEmpty())
				xpathChild = child.getTagName();
			else
				xpathChild = child.getTagName() + "/" + xpathChild;

		} else {
			if (xpathChild.isEmpty())
				xpathChild = child.getTagName() + "[" + getIndex(elements, child) + "]";
			else
				xpathChild = child.getTagName() + "[" + getIndex(elements, child) + "]" + "/" + xpathChild;
		}
		return xpathChild;
	}

	private static int getIndex(List<WebElement> elementSiblings, WebElement element) {
		int count = 1;
		for (Iterator iterator = elementSiblings.iterator(); iterator.hasNext();) {
			WebElement currentElement = (WebElement) iterator.next();
			String a = currentElement.getTagName();
			String b = element.getTagName();
			if (currentElement.getTagName().equalsIgnoreCase(element.getTagName())) {
				if (!currentElement.equals(element)) {
					count++;
				} else
					return count;
			}
		}
		return -1;
	}
}
