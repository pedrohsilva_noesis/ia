package service;

import java.sql.SQLException;

import dao.RewardDAO;
import model.Reward;

public class RewardService {
	RewardDAO dao;

	public RewardService() {
		try {
			dao = new RewardDAO();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void persist(Reward r, boolean updateAttempts) {
		if(!existReward(r))
			dao.persistNewReward(r);
		else
			dao.updateReward(r, updateAttempts);
	}

	private boolean existReward(Reward r) {
		return dao.existReward(r);
	}
	
	public Reward getBiggerReward(int id_test_step) {
		return dao.getBiggerReward(id_test_step);
	}	
	
	public Reward getLessUsedReward(int id_test_step) {
		return dao.getLessUsedReward(id_test_step);
	}
	
}
