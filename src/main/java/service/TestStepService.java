package service;

import java.util.List;

import dao.TestStepDAO;
import model.TestStep;

public class TestStepService {
	TestStepDAO testStepDAO;

	public List<Integer> getMonitoredTests(){
		return testStepDAO.getMonitoredTests();
	}

	public List<TestStep> getTestStep(Integer idTest) {
		return testStepDAO.getTestStep(idTest);
	}
}
