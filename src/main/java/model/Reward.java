package model;

public class Reward {
	int id_ia;
	String old_xpath;
	String new_xpath;
	int attempts;
	int reward;
	int fk_id_test_step;
	boolean is_activated;// tinyint;

	public Reward(String old_xpath, String new_xpath, int reward, int fk_id_test_step) {
		this.old_xpath = old_xpath;
		this.new_xpath = new_xpath;
		this.reward = reward;
		this.fk_id_test_step = fk_id_test_step;
	}

	public int getId_ia() {
		return id_ia;
	}

	public void setId_ia(int id_ia) {
		this.id_ia = id_ia;
	}

	public String getOld_xpath() {
		return old_xpath;
	}

	public void setOld_xpath(String old_xpath) {
		this.old_xpath = old_xpath;
	}

	public String getNew_xpath() {
		return new_xpath;
	}

	public void setNew_xpath(String new_xpath) {
		this.new_xpath = new_xpath;
	}

	public int getAttempts() {
		return attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	public int getReward() {
		return reward;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public int getFk_id_test_step() {
		return fk_id_test_step;
	}

	public void setFk_id_test_step(int fk_id_test_step) {
		this.fk_id_test_step = fk_id_test_step;
	}

	public boolean isIs_activated() {
		return is_activated;
	}

	public void setIs_activated(boolean is_activated) {
		this.is_activated = is_activated;
	}

}
