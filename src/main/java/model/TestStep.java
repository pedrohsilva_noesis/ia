package model;

public class TestStep {
	int step_id;
	int fk_object_id;
	String xpath;
	String xpath_iframe;

	public int getStep_id() {
		return step_id;
	}

	public void setStep_id(int step_id) {
		this.step_id = step_id;
	}

	public int getFk_object_id() {
		return fk_object_id;
	}

	public void setFk_object_id(int fk_object_id) {
		this.fk_object_id = fk_object_id;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public String getXpath_iframe() {
		return xpath_iframe;
	}

	public void setXpath_iframe(String xpath_iframe) {
		this.xpath_iframe = xpath_iframe;
	}

}
